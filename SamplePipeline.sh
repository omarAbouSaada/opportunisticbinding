#!/bin/bash

dataFormat=$1
    
sampleFile=/data/teamreic/SAMPLEFILE

read -p "Sample name: " sampleName
read -p "Genome Assembly: " genomeAssembly
read -p "Sample Control: " sampleControl
read -p "Path: " path
read -p "GSE Number: " GESNumber
read -p "GSM Number: " GSMNumber
if [[ $dataFormat = "SRR" ]];then
    read -p "SRR Number: " SRRNumber
fi
read -p "Comments: " comments
read -p "Open HTML file when QC ends? (y/n): " toSeeOrNotToSee

MAIN_PATH=/data/teamreic
MAIN_DATA_PATH=${MAIN_PATH}/$path
DATA_PATH=${MAIN_DATA_PATH}/data
QC_PATH=${MAIN_DATA_PATH}/quality

mkdir -p $DATA_PATH

duplicate=$(cat $sampleFile | grep ^$sampleName$'\t')

if  [[ ! -z $duplicate ]];then
    echo $sampleName already exists, try incrementing the replicate number.>&2
    exit 1
fi

if [[ $dataFormat = "fastq" ]] || [[ $dataFormat = "FASTQ" ]];then
    echo ${sampleName}$'\tFASTQ\t'${genomeAssembly}$'\t'${sampleControl}$'\t'${path}$'\t'${GESNumber}$'\t'${GSMNumber}$'\t'${SRRNumber}$'\t'${USER}$'\t'${comments} >> $sampleFile
fi

if [[ $dataFormat = "srr" ]] || [[ $dataFormat = "SRR" ]];then

    #In case there are multiple SRRs to download
    SRRs=$(echo $SRRNumber | sed 's/,/\n/g')
    lines=$(echo "$SRRs" | wc -l)

    if [[ $lines = "1" ]];then
        getSRA.sh -v $DATA_PATH $sampleName ${SRRs[0]}
        if [ -e $DATA_PATH/${sampleName}.sra ]; then
            echo ${sampleName}$'\tDownloaded\t'${genomeAssembly}$'\t'${sampleControl}$'\t'${path}$'\t'${GESNumber}$'\t'${GSMNumber}$'\t'${SRRNumber}$'\t'${USER}$'\t'${comments} >> $sampleFile
        else
            echo $sampleName was not successfully downloaded.>&2
            exit 1
        fi

        # FASTQ-DUMP PROCESSING
        fastq-dumping.sh $sampleName $DATA_PATH
        if [ -e ${DATA_PATH}/${sampleName}.fastq.gz ]; then
            tempFile=$(mktemp)
            cat $sampleFile  | awk -v OFS="\t" -v var=$sampleName '$1==var {$2="FASTQ";print $0} $1!=var {print $0}'> $tempFile
            mv $tempFile $sampleFile
        else
            echo $sampleName failed the fastq-dump operation.>&2
            exit 1
        fi

    else #Multiple SRR handling

        i=1
     
        for SRR in $SRRs
        do
            getSRA.sh -v $DATA_PATH ${sampleName}"_"${i} $SRR
            if [ ! -e ${DATA_PATH}/${sampleName}"_"${i}.sra ]; then
                echo $sampleName was not fully downloaded. One of the SRRs might be missing.>&2
                exit 1
            fi
            ((i++))
        done

        echo ${sampleName}$'\tDownloaded\t'${genomeAssembly}$'\t'${sampleControl}$'\t'${path}$'\t'${GESNumber}$'\t'${GSMNumber}$'\t'${SRRNumber}$'\t'${USER}$'\t'${comments} >> $sampleFile

        # FASTQ-DUMP PROCESSING FOR MULTIPLE SRRs
        i=1

        for SRR in $SRRs
        do
            fastq-dumping.sh ${sampleName}"_"${i} $DATA_PATH
            if [ ! -e ${DATA_PATH}/${sampleName}"_"${i}.fastq.gz ]; then
                echo $sampleName failed the fastq-dump operation.>&2
                exit 1
            fi
            ((i++))
        done

        for partialFile in ${DATA_PATH}/${sampleName}_*
        do
            cat $partialFile >> ${DATA_PATH}/${sampleName}.fastq.gz
            rm $partialFile
        done

        tempFile=$(mktemp)
        cat $sampleFile  | awk -v OFS="\t" -v var=$sampleName '$1==var {$2="FASTQ";print $0} $1!=var {print $0}'> $tempFile
        mv $tempFile $sampleFile
    fi
fi

#FASTQ-DUMPS
if  [[ $SRRNumber = "1" ]];then
    fasterqc.sh ${sampleName}.fastq.gz $toSeeOrNotToSee $MAIN_DATA_PATH
    if [ ! -e ${QC_PATH}/${sampleName}.fastqc.html ]; then
        echo $sampleName was not properly processed, there is no ${sampleName}.fastqc.html file.
        exit 1
    fi
else
    fasterqc.sh ${sampleName}_1.fastq.gz $toSeeOrNotToSee $MAIN_DATA_PATH
    if [ ! -e ${QC_PATH}/${sampleName}_1.fastqc.html ]; then
        echo $sampleName was not properly processed, there is no ${sampleName}.fastqc.html file.
        exit 1
    fi
    fasterqc.sh ${sampleName}_2.fastq.gz $toSeeOrNotToSee $MAIN_DATA_PATH
    if [ ! -e ${QC_PATH}/${sampleName}_2.fastqc.html ]; then
        echo $sampleName_2 was not properly processed, there is no ${sampleName}.fastqc.html file.
        exit 1
    fi
fi
    

else
    echo $sampleName has gone through QC.
    tempFile=$(mktemp)
    cat $sampleFile  | awk -v OFS="\t" -v var=$sampleName '$1==var {$2="QC";print $0} $1!=var {print $0}' > $tempFile
    mv $tempFile $sampleFile
fi

