#!/bin/bash

DATA_PATH=$2
SAMPLE_NAME=$3
SRA_ID=$4

verbose=1

while getopts ":v:q" opt; do
    case $opt in
        v)
            verbose=1
            ;;
        q)
            verbose=0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            ;;
    esac
done

if [ ! -e $DATA_PATH/$SAMPLE_NAME.sra ]; then

    rm /data/${USER}/tmp/sra/${SRA_ID}.sra.lock 2> /dev/null
    rm /data/${USER}/tmp/sra/${SRA_ID}.sra.*.tmp 2> /dev/null

    prefetch -X 200G $SRA_ID >/dev/null 2>&1 #Silences prefetch's outputs. They're useless.

    if [ -e /data/${USER}/tmp/sra/${SRA_ID}.sra ]; then
        mv /data/${USER}/tmp/sra/${SRA_ID}.sra ${DATA_PATH}/${SAMPLE_NAME}.sra
        if [[ $verbose -eq 1 ]]; then
            echo $SAMPLE_NAME has been successfully downloaded.
        fi
    else
        echo $SAMPLE_NAME could not be downloaded. Check if $SRA_ID is a correct SRA identifier. >&2
        exit 1
    fi
else
    echo $SAMPLE_NAME is already present in ${DATA_PATH}.
    exit 1
fi

