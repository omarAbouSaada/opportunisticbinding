**OpportunisticBinding**

This is the data pre-processing code that I wrote for the final internship of my Master's degree.

In this internship I was interested in training a machine learning algorithm (an SVM in this case) to attempt to determine if the binding of the NRF1 Transcription Factor (TF) is dependent on the presence of other TFs that "clear the way" in some sort and allow NRF1 to bind.
That meant attempting to find out if there are are TF motifs associated with the NRF1 TF's binding to a given region by training an SVM to predict NRF1 binding sites based on the presence or absence of other TF motifs nearby.

The script samplePipeline.sh was written in order to download and automatically pre-process NGS data from the NCBI.
It also updates a text file named SAMPLEFILE which contains data relative to all of the samples we download and what tools we have used on them.

It uses three scripts that are basically wrappers of other tools to automate the process and report errors as they happen.
These scripts are getSRA.sh, fastq-dumping.sh and fasterqc.sh

Finally, the file named FeatureFileGeneration contains the code that was used to turn ChIP-seq datasets into a feature file that can be used to train a machine learning algorithm. It wasn't deployed as a pipeline since it was constantly evolving as we determined how to properly build a ground truth dataset, so it's a lot more rough around the edges than the other code.