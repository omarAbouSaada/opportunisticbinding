#!/bin/bash

filename=${1:0:-3}
toSeeOrNotToSee=$2
DATA_PATH=$3

tempDir=$(mktemp -d)

if [ ! -e ${DATA_PATH}/data/${filename}.gz ]; then
    echo ${filename}.gz does not exist. >&2
    exit 1
else
    if [ -e ${DATA_PATH}/quality/${filename}c.html ]; then
        echo ${filename}c.html exists - the data has already been processed.
        exit 1
    else
        zcat ${DATA_PATH}/data/${filename}.gz | fastqc -o $tempDir stdin
        mv ${tempDir}/stdin_fastqc.html ${DATA_PATH}/quality/${filename}c.html
        mv ${tempDir}/stdin_fastqc.zip ${DATA_PATH}/quality/${filename}c.zip
        rm ${DATA_PATH}/quality/${filename}c.zip
        if [[ $toSeeOrNotToSee == "y" ]];then
            see ${DATA_PATH}/quality/${filename}c.html
        fi
    fi
fi


