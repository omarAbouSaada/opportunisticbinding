#!/bin/bash

SRA_ID=$1
DATA_PATH=$2

if [ ! -e $DATA_PATH/$SRA_ID.fastq.gz ]; then
    if [ -s $DATA_PATH/$SRA_ID.sra ]; then
        fastq-dump $DATA_PATH/$SRA_ID.sra --outdir $DATA_PATH --gzip --split-3 >/dev/null 2>&1
        if [ -e $DATA_PATH/$SRA_ID.fastq.gz ]; then
            echo $SRA_ID has been successfully processed.
            rm $DATA_PATH/$SRA_ID.sra
        else
            echo $SRA_ID: Something went wrong during processing.
            exit 1
        fi
    else
        echo $SRA_ID.sra does not exist or is an empty file and there is no processed file with the $SRA_ID name.>&2
        exit 1
    fi
else
    echo $SRA_ID has already been processed and compressed.>&2
    exit 1
fi


